/**
 * Created by Gareth1305 on 20/10/2015.
 */
public class SeatsFullException extends Exception {

    public SeatsFullException(String message) {
        super(message);
    }

}