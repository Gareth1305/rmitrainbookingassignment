import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

/**
 * Created by Gareth1305 on 20/10/2015.
 * In my server class I have created 4 users. Each user has a username and they use
 * this username to log into the system to book a seat.
 *
 * If there is no seats available I throw a custom made exception called SeatsFullException.
 * This will return to us, the exception message.
 *
 * I have also implemented it, so that a user can only book 1 seat per journey. This is
 * to avoid double booking. I have done this by creating a List of strings(possibleRoutes)
 * in the user class. When the user makes their selection. The selected route is then removed
 * from the list. Once the user makes thier selection, the list is checked and if the selection is in
 * the list, the user can book the route
 */
public class TrainClient {

    Train stub;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    private TrainClient() {

    }


    public static void main(String[] args) {
        TrainClient client = new TrainClient();
        client.openConnection();
    }


    public void openConnection() {
        try {
            Registry registry = LocateRegistry.getRegistry("localhost", 1098);
            stub = (Train) registry.lookup("TrainObj");
            System.out.println("*********Welcome to the Train Booking System***********");
            AskForUserName();
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            return;
        }
    }


    private void AskForUserName() throws IOException {

        System.out.println("\nEnter Your Username To Book A Ticket: ");
        String inputedUser = in.readLine();
        if (stub.validateUsername(inputedUser)) {
            System.out.println("\nWe have found " + inputedUser + " in our database");
            stub.setCurrentUser(inputedUser);
            printMenu();
        } else {
            System.out.println("\nWe have not found " + inputedUser + " in our database");
        }
    }


    private void printMenu() throws IOException {
        List<Route> routes = stub.getRoutes();
        System.out.println("Please select a route by entering the route number: (N - to exit) ");
        for (int i = 0; i < routes.size(); i++) {
            System.out.println(routes.get(i).getDestination() + " (" + (i + 1) + ") \t\t " +
                    stub.getRemainingSpaces(i) + " spaces remaining.");
        }
        parseUserInput();
    }


    private void parseUserInput() throws IOException {
        String selectedRoute = in.readLine();
        if (selectedRoute.equalsIgnoreCase("N")) {
            GoodBye();
        } else {
            int route = 0;
            try {
                route = Integer.parseInt(selectedRoute);
            } catch (NumberFormatException e) {
                System.out.println("Invalid route entered! Please try again.");
                printMenu();
            }
            String routeDest = stub.getRouteDestination(route - 1);
            boolean canBook = stub.checkIfUserIsDoubleBooking(stub.getCurrentUser(), routeDest, stub.getPotentialRoutes(stub.getCurrentUser()));

            if (canBook) {
                if (route == 1 || route == 2 || route == 3)
                    bookASeat(route - 1);
                else {
                    System.out.println("Invalid route entersed! Please try again.");
                    printMenu();
                }
            } else {
                System.out.println("You are attempting to double book. This is not allowed!");
                printMenu();
            }
        }
    }


    private void bookASeat(int route) throws IOException {
        try {
            stub.bookASeat(route);
            stub.doNotAllowDoubleBooking(stub.getCurrentUser(), stub.getRouteDestination(route));

            System.out.println(stub.getCurrentUser().getUsername() + " booked a ticket to " + stub.getRouteDestination(route) +
                    "\nThere are " + stub.getRemainingSpaces(route) + " spaces left on this route.");
        } catch (SeatsFullException e) {
            System.out.println(e.getMessage());
        }
        if (anotherTicket()) {
            printMenu();
        } else {
            System.out.println("Goodbye");
        }
    }


    private boolean anotherTicket() throws IOException {
        System.out.println("Do you want to book another ticket? (Y/N)");
        String userInput = in.readLine();
        if (userInput.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }

    private void GoodBye() {
        System.out.print("Thank you for booking with us! Goodbye!");
    }

}