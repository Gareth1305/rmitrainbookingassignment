import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gareth1305 on 20/10/2015.
 */
public class TrainServer implements Train {

    private List<Route> routes = new ArrayList<Route>();
    private List<User> users = new ArrayList<User>();
    private User currentUser;

    public TrainServer() {
        routes.add(new Route("Dublin-Cork"));
        routes.add(new Route("Dublin-Galway"));
        routes.add(new Route("Dublin-Belfast"));
        users.add(new User("Gareth"));
        users.add(new User("Conor"));
        users.add(new User("Glenn"));
        users.add(new User("Ciara"));
    }

    public static void main(String[] args) throws IOException {
        try {
            TrainServer server = new TrainServer();
            Train stub = (Train) UnicastRemoteObject.exportObject(server, 0);
            Registry registry = LocateRegistry.createRegistry(1098);
            registry.bind("TrainObj", stub);
            System.out.println("Server running, waiting on client connection....");
        } catch (Exception e) {
            System.err.println("Server error:" + e.toString());
        }
    }

    @Override
    public void bookASeat(int route) throws SeatsFullException, RemoteException {
        routes.get(route).bookASeat();
    }

    @Override
    public String getRouteDestination(int route) throws RemoteException {
        return routes.get(route).getDestination();
    }

    @Override
    public List<Route> getRoutes() throws RemoteException {
        return routes;
    }

    @Override
    public int getRemainingSpaces(int route) throws RemoteException {
        return routes.get(route).getRemainingSpaces();
    }

    @Override
    public boolean validateUsername(String inputedUser) {
        boolean found = false;
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(inputedUser)) {
                found = true;
                break;
            }
        }
        return found;
    }

    // When a user logs in, the Current user is set
    @Override
    public void setCurrentUser(String inputedUser) throws RemoteException {
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(inputedUser)) {
                currentUser = user;
                break;
            }
        }
    }

    @Override
    public User getCurrentUser() throws RemoteException {
        return currentUser;
    }

    @Override
    public ArrayList<String> getPotentialRoutes(User currentUser) throws RemoteException {
        return currentUser.getPotentialRoutes();
    }

    // The below method will remove the selected route from the arraylist in the user class
    @Override
    public void doNotAllowDoubleBooking(User currentUser, String routeDestination) throws RemoteException, SeatsFullException {
        for (int i = 0; i < users.size(); i++) {
            if (currentUser.getUsername().equalsIgnoreCase(users.get(i).getUsername())) {
                users.get(i).removeArrayItem(routeDestination);
            }
        }
    }

    // The below method will check for double booking. If the user has already booked the route, false will be returned
    @Override
    public boolean checkIfUserIsDoubleBooking(User currentUser, String routeDest, List<String> routes) throws RemoteException {
        boolean canBook = false;
        for (String s : routes) {
            if (routeDest.equalsIgnoreCase(s)) {
                canBook = true;
                break;
            }
        }
        return canBook;
    }

}