import java.io.Serializable;

/**
 * Created by Gareth1305 on 20/10/2015.
 */
public class Route implements Serializable {

    private String destination;
    private int remainingSpaces = 3;

    public Route(String destination) {
        this.destination = destination;
    }

    // Once a seat is booked, the count of the number of seats for that route decreases by 1
    public synchronized void bookASeat() throws SeatsFullException {
        if (remainingSpaces > 0) {
            remainingSpaces--;
        } else {
            throw new SeatsFullException("Sorry, this route is fully booked");
        }
    }

    public String getDestination() {
        return destination;
    }

    public synchronized int getRemainingSpaces() {
        return remainingSpaces;
    }

}
