import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Gareth1305 on 20/10/2015.
 */
public class User implements Serializable {

    private String username;
    ArrayList<String> potentialRoutes = new ArrayList<String>(Arrays.asList("Dublin-Cork", "Dublin-Galway", "Dublin-Belfast"));

    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public synchronized ArrayList<String> getPotentialRoutes() {
        return potentialRoutes;
    }

    //A method which will remove the selected route form the above List
    public synchronized void removeArrayItem(String route) throws SeatsFullException {
        if (potentialRoutes != null) {
            for (String aRoute : potentialRoutes) {
                if (aRoute.equalsIgnoreCase(route)) {
                    potentialRoutes.remove(aRoute);
                    break;
                }
            }
        }
    }
}
