import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gareth1305 on 20/10/2015.
 */
public interface Train extends Remote {

    public void bookASeat(int route) throws SeatsFullException, RemoteException;

    public String getRouteDestination(int route) throws RemoteException;

    public List<Route> getRoutes() throws RemoteException;

    public int getRemainingSpaces(int route) throws RemoteException;

    public boolean validateUsername(String inputedUser) throws RemoteException;

    public void setCurrentUser(String inputedUser) throws RemoteException;

    public User getCurrentUser() throws RemoteException;

    public ArrayList<String> getPotentialRoutes(User currentUser) throws RemoteException;

    public void doNotAllowDoubleBooking(User currentUser, String routeDestination) throws RemoteException, SeatsFullException;

    public boolean checkIfUserIsDoubleBooking(User currentUser, String selectedRoute, List<String> routes) throws RemoteException;
}